package com.jarupong.afinal

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.jarupong.afinal.R
import com.jarupong.afinal.data.DataSource
import com.jarupong.afinal.data.DataSource.clear
import com.jarupong.afinal.data.DataSource.deleteItem
import com.jarupong.afinal.data.DataSource.games
import com.jarupong.afinal.data.DataSource.sellItem
import com.jarupong.afinal.data.DataSource.updateItem
import com.jarupong.afinal.databinding.FragmentItemDetailBinding
import com.jarupong.afinal.model.Game
import java.nio.file.Files.size
import java.text.FieldPosition

class ItemDetailFragment : Fragment() {
    private  val navigationArgs: ItemDetailFragmentArgs by navArgs()
    private var _binding: FragmentItemDetailBinding? = null
    private val binding get() = _binding
    private lateinit var game: Game


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentItemDetailBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = navigationArgs.itemId

        for(games in DataSource.games){
            if(id == games.itemId){
                game = games
            }
        }
//        binding?.image?.setImageResource(game.imageResourceId)
        binding?.itemName?.text = game.itemName
        binding?.itemPrice?.text = "$" + game.itemPrice.toString()
        binding?.itemCount?.text = game.quantityInStock.toString()

        binding?.editItem?.setOnClickListener{
            editItem(id)
        }
        binding?.deleteItem?.setOnClickListener {
            showConfirmationDialog(id)
        }
        binding?.sellItem?.setOnClickListener {
            sellItem(id-1)
            onViewCreated(view, savedInstanceState)
        }
    }
    private fun editItem(id: Int) {
        for(games in DataSource.games){
            if(id == games.itemId){
                game = games
            }
        }
        binding?.itemName?.setText(game.itemName)
        binding?.itemPrice?.setText(game.itemPrice.toString())
        binding?.itemCount?.setText(game.quantityInStock.toString())

        val action = ItemDetailFragmentDirections.actionItemDetailFragmentToAddItemFragment(
            getString(R.string.edit_fragment_title),
            itemId = id
        )
        this.findNavController().navigate(action)
    }

    private fun showConfirmationDialog(position: Int) {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(android.R.string.dialog_alert_title))
            .setMessage(getString(R.string.delete_question))
            .setCancelable(false)
            .setNegativeButton(getString(R.string.no)) { _, _ -> }
            .setPositiveButton(getString(R.string.yes)) { _, _ ->
                deleteItem(position)
                val action = ItemDetailFragmentDirections.actionItemDetailFragmentToItemListFragment()
                this.findNavController().navigate(action)
            }
            .show()
    }

}