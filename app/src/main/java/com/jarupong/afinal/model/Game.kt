package com.jarupong.afinal.model

import androidx.annotation.DrawableRes

data class Game(
    val itemId: Int,
    val itemName: String,
    val itemPrice: Double,
    var quantityInStock: Int
)
