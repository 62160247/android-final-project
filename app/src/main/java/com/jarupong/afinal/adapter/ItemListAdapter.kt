package com.jarupong.afinal.adapter

import android.content.ClipData
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jarupong.afinal.R
import com.jarupong.afinal.data.DataSource
import com.jarupong.afinal.databinding.ItemListItemBinding
import com.jarupong.afinal.model.Game

class ItemListAdapter(val context: Context,private val dataList:List<Game>, private val onItemClicked: (Game) -> Unit):
    RecyclerView.Adapter<ItemListAdapter.ItemViewHolder>(){
        class ItemViewHolder(private val view: View):
                RecyclerView.ViewHolder(view){
                    val name : TextView = view.findViewById(R.id.item_name)
                    val price : TextView = view.findViewById(R.id.item_price)
                    val quantity : TextView = view.findViewById(R.id.item_quantity)
                }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val adapterLayout = LayoutInflater.from(
            parent.context
        ).inflate(R.layout.item_list_item,parent,false)
        return ItemViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
            val item = dataList [position]
            holder.name.text = item.itemName
            holder.price.text = "$" + item.itemPrice.toString()
            holder.quantity.text = item.quantityInStock.toString()
            holder.itemView.setOnClickListener{
                onItemClicked(item)
            }
    }

    override fun getItemCount() = dataList.size
}