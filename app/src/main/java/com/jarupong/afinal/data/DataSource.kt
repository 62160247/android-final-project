/*
* Copyright (C) 2021 The Android Open Source Project.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package com.jarupong.afinal.data

import com.jarupong.afinal.model.Game

/**
 * An object to generate a static list of dogs
 */
object DataSource {
var games:MutableList<Game> = mutableListOf(
        Game(
            1,
            "GTA V",
            10.00,
            10
        ),
        Game(
            2,
            "Slay the Spire",
            3.99,
            10
        ),
        Game(
            3,
            "Hades",
            3.99,
            10
        ),
        Game(
            4,
            "Dead Cells",
            3.99,
            10
        ),
        Game(
            5,
            "ELDEN RING",
            3.99,
            10
        ),
        Game(
            6,
            "Risk of Rain 2",
            2.99,
            10
        )
    )
    fun addNewItem(game: Game) {
        games.add(game)
    }
    fun getItem(pos:Int): Game {
        return games[pos]
    }
    fun updateItem(game: Game, pos: Int) {
        games[pos] = game
    }
    fun deleteItem(pos: Int) {
        for (i in 0..games.size-1){
            if(pos == games.get(i).itemId){
                games.removeAt(i)
                break
            }
        }
    }
    fun sellItem(pos: Int){
        if (games[pos].quantityInStock > 0){
            games[pos].quantityInStock --
        }
    }
    fun clear(){
        games.clear()
    }

}
