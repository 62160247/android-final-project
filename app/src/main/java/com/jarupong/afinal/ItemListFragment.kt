package com.jarupong.afinal

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jarupong.afinal.R
import com.jarupong.afinal.adapter.ItemListAdapter
import com.jarupong.afinal.data.DataSource
import com.jarupong.afinal.databinding.FragmentItemListBinding
import com.jarupong.afinal.model.Game

class ItemListFragment : Fragment() {
    private var _binding: FragmentItemListBinding? = null
    private val binding get() = _binding;
    private lateinit var recyclerView:RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentItemListBinding.inflate(inflater, container, false)
        val view = binding?.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val data = DataSource.games
        recyclerView = binding?.recyclerView!!
        if(data.size > 0){
            recyclerView.adapter = ItemListAdapter(requireContext(),data){
                val action = ItemListFragmentDirections.actionItemListFragmentToItemDetailFragment(itemId = it.itemId)
                view.findNavController().navigate(action)
            }

            recyclerView.layoutManager = LinearLayoutManager(requireContext())
        }

        binding?.floatingActionButton?.setOnClickListener {
            val action = ItemListFragmentDirections.actionItemListFragmentToAddItemFragment(
                getString(R.string.add_fragment_title)
            )
            this.findNavController().navigate(action)
        }
    }

}