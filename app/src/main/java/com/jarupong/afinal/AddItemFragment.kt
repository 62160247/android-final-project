package com.jarupong.afinal

import android.os.Bundle
import android.provider.SyncStateContract.Helpers.update
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.RecyclerView
import com.jarupong.afinal.R
import com.jarupong.afinal.data.DataSource
import com.jarupong.afinal.data.DataSource.addNewItem
import com.jarupong.afinal.data.DataSource.updateItem
import com.jarupong.afinal.databinding.FragmentAddItemBinding
import com.jarupong.afinal.databinding.FragmentItemListBinding
import com.jarupong.afinal.model.Game


class AddItemFragment : Fragment() {
    private val navigationArgs: AddItemFragmentArgs by navArgs()
    private var _binding: FragmentAddItemBinding? = null
    private val binding get() = _binding;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAddItemBinding.inflate(inflater, container, false)
        val view = binding?.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = navigationArgs.itemId
        if (id > 0){
            var game = DataSource.getItem(id-1)
            binding?.itemName?.setText(game.itemName)
            binding?.itemPrice?.setText(game.itemPrice.toString())
            binding?.itemCount?.setText(game.quantityInStock.toString())
        }
        binding?.saveAction?.setOnClickListener {
            if (id > 0){
                updateItems(id)
            }else {
                addItem()
            }
        }
        binding?.cancelAction?.setOnClickListener {
            val action = AddItemFragmentDirections.actionAddItemFragmentToItemListFragment()
            findNavController().navigate(action)
        }
    }

    private fun updateItems(id:Int) {
        var name = binding?.itemName?.text.toString()
        var price = binding?.itemPrice?.text.toString().toDouble()
        var quantity = binding?.itemCount?.text.toString().toInt()
        val update = Game(id,name,price,quantity)
        updateItem(update,id-1)
        val action = AddItemFragmentDirections.actionAddItemFragmentToItemListFragment()
        findNavController().navigate(action)
        }

    private fun addItem() {
        var name = binding?.itemName?.text.toString()
        var price = binding?.itemPrice?.text.toString().toDouble()
        var quantity = binding?.itemCount?.text.toString().toInt()
        val newGame = Game(DataSource.games.size+1,name,price,quantity)
        addNewItem(newGame)
        val action = AddItemFragmentDirections.actionAddItemFragmentToItemListFragment()
        findNavController().navigate(action)
    }
}